package Desafios;

public class Desafio07 {

	public static void main(String[] args) {

		// FUNCIONES TRIGONOMETRICAS
				double a = Math.sin(Math.toRadians(45)); //sin
				System.out.println(a);

				double b = Math.cos(Math.toRadians(45)); //cos
				System.out.println(b);
				
				double c = Math.tan(Math.toRadians(45)); //tan
				System.out.println(c);
				
				double d = Math.atan(Math.toRadians(45)); //atan
				System.out.println(d);
				
				double e = Math.atan(Math.toRadians(45)); //atan2
				System.out.println(e);
				
				// FUNCION EXPONENCIAL E INVERSA
				double f = Math.exp(45); //exp
				System.out.println(f);
				
				double g = Math.log(45); //log
				System.out.println(g);
				
				// PI & e
				System.out.println(Math.PI); //PI
				
				System.out.println(Math.E); //e
				
	}

}
