package Desafios;

import java.util.Scanner;

public class Desafio08 {

	public static void main(String[] args) {

		Scanner s = new Scanner(System.in);
		
		System.out.print("Introducir un numero: ");
		double num = s.nextDouble();
		
		System.out.println("La raiz de "+num+ " es: "+Math.sqrt(num)); //sqrt == Raiz cuadrada

	}

}
