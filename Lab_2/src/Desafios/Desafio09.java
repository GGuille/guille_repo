package Desafios;

import java.util.Scanner;

public class Desafio09 {

	public static void main(String[] args) {

		Scanner s = new Scanner(System.in);

		System.out.print("Ingrese su altura en centimetros: ");
		int cm = s.nextInt();

		System.out.print("Ingrese su genero hombre/mujer: ");
		String gen = s.next();
		
		System.out.println();
		
		if (gen.equals("hombre")) {
			cm -= 110;
			System.out.println("Su peso ideal es: " + cm);
		} else if (gen.equals("mujer")) {
			cm -= 120;
			System.out.println("Su peso ideal es: " + cm);
		} else
			System.out.println("genero incorrecto");

	}

}
