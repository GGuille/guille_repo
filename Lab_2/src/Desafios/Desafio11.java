package Desafios;
import java.util.Random;

public class Desafio11 {

	public static void main(String[] args) {
		cuentaCorriente cuenta1 = new cuentaCorriente("pepe", 200);
		cuentaCorriente cuenta2 = new cuentaCorriente("carlitos", 1500);
		
		System.out.println(cuenta1.ingresoDireno(600)+"\n");//ingreso plata
		cuentaCorriente.transferencia(500, cuenta1, cuenta2);//rtanfiero
		
		System.out.println(cuenta1.toString());//muestro
		System.out.println(cuenta2.toString());
		
	}

}

class cuentaCorriente{
	
	private double saldo;
	private String nombreTitular;
	private long numeroCuenta;
	
	public cuentaCorriente(String nombreTitular, double saldo) {		//Constructor
		this.nombreTitular = nombreTitular;
		this.saldo = saldo;
		
		Random random = new Random();//punchi
		this.numeroCuenta = Math.abs(random.nextLong());//numero random de la cuenta
		
	}
	
	public String ingresoDireno(double money) {				//setter ingreso de dinero
		if(money > 0) {
			this.saldo += money;
			return "ha ingresado: "+money+", a la cuenta a nombre de: "+this.nombreTitular;
		}else
			return "Usted no puede hacer semejante barbaridad, debe ingresar dinero";
	}
		
	public void sacarDinero(double dinero) {	//adonde se va la plata? quien sabe
		this.saldo -= dinero;
	}
	
	public static void transferencia(double plata, cuentaCorriente primeraCta, cuentaCorriente segundaCta) { //Transferemcia de primera a segunda
		primeraCta.saldo -= plata;//	"sale"
		segundaCta.saldo += plata;//	"entra"
	}

	@Override
	public String toString() {
		return "cuentaCorriente [saldo=" + saldo + ", nombreTitular=" + nombreTitular + ", numeroCuenta=" + numeroCuenta
				+ "]";
	}
		
		

}
