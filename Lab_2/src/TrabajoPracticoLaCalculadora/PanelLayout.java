package TrabajoPracticoLaCalculadora;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

class PanelLayout extends JPanel implements ActionListener {

	JTextField num = new JTextField();
	JTextField res = new JTextField();
	JButton menos = new JButton("-");
	JButton mas = new JButton("+");
	JButton mult = new JButton("*");
	JButton div = new JButton("/");
	JButton siete = new JButton("7");
	JButton ocho = new JButton("8");
	JButton nueve = new JButton("9");
	JButton borrar = new JButton("C");
	JButton cuatro = new JButton("4");
	JButton cinco = new JButton("5");
	JButton seis = new JButton("6");
	JButton punto = new JButton(".");
	JButton uno = new JButton("1");
	JButton dos = new JButton("2");
	JButton tres = new JButton("3");
	JButton igual = new JButton("=");
	JButton cero = new JButton("0");
	JButton not = new JButton("?");

	public PanelLayout() {
		setLayout(new FlowLayout());
		

		num.setBounds(0, 0, 245, 50);
		num.setFont(new Font("Tahoma", Font.BOLD, 20)); // text numero
		add(num);

		res.setBounds(0, 50, 245, 50);
		res.setFont(new Font("Tahoma", Font.BOLD, 20)); // text Resultado
		add(res);

		menos.setBounds(0, 100, 60, 60);
		add(menos); // MENOS -
		menos.addActionListener(this);

		mas.setBounds(62, 100, 60, 60);
		add(mas); // MAS +
		mas.addActionListener(this);

		mult.setBounds(124, 100, 60, 60);
		add(mult); // MULTIPLICACION *
		mult.addActionListener(this);

		div.setBounds(186, 100, 60, 60);
		add(div); // DIVICION /
		div.addActionListener(this);

		siete.setBounds(0, 162, 60, 60);
		add(siete); // SIETE
		siete.addActionListener(this);

		ocho.setBounds(62, 162, 60, 60);
		add(ocho); // OCHO
		ocho.addActionListener(this);

		nueve.setBounds(124, 162, 60, 60);
		add(nueve); // NUEVE
		nueve.addActionListener(this);

		borrar.setBounds(186, 162, 60, 60);
		add(borrar); // BORRAR
		borrar.addActionListener(this);

		cuatro.setBounds(0, 224, 60, 60);
		add(cuatro); // CUATRO
		cuatro.addActionListener(this);

		cinco.setBounds(62, 224, 60, 60);
		add(cinco); // CINCO
		cinco.addActionListener(this);

		seis.setBounds(124, 224, 60, 60);
		add(seis); // SEIS
		seis.addActionListener(this);

		punto.setBounds(0, 348, 60, 60);
		add(punto); // PUNTO
		punto.addActionListener(this);

		uno.setBounds(0, 286, 60, 60);
		add(uno); // UNO
		uno.addActionListener(this);

		dos.setBounds(62, 286, 60, 60);
		add(dos); // DOS
		dos.addActionListener(this);

		tres.setBounds(124, 286, 60, 60);
		add(tres); // TRES
		tres.addActionListener(this);

		igual.setBounds(124, 348, 120, 60);
		add(igual); // IGUAL
		igual.addActionListener(this);

		cero.setBounds(62, 348, 60, 60);
		add(cero); // CERO
		cero.addActionListener(this);

		not.setBounds(186, 224, 60, 60);
		add(not); // ??????
		not.addActionListener(this);
		
		

	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == uno) {
			if (num.getText() == "")
				num.setText("1");
			else
				num.setText(num.getText() + "1");
		} else if (e.getSource() == dos) {
			if (num.getText() == "")
				num.setText("2");
			else
				num.setText(num.getText() + "2");
		} else if (e.getSource() == tres) {
			if (num.getText() == "")
				num.setText("3");
			else
				num.setText(num.getText() + "3");
		} else if (e.getSource() == cuatro) {
			if (num.getText() == "")
				num.setText("4");
			else
				num.setText(num.getText() + "4");
		} else if (e.getSource() == cinco) {
			if (num.getText() == "")
				num.setText("5");
			else
				num.setText(num.getText() + "5");
		} else if (e.getSource() == seis) {
			if (num.getText() == "")
				num.setText("6");
			else
				num.setText(num.getText() + "6");
		} else if (e.getSource() == siete) {
			if (num.getText() == "")
				num.setText("7");
			else
				num.setText(num.getText() + "7");
		} else if (e.getSource() == ocho) {
			if (num.getText() == "")
				num.setText("8");
			else
				num.setText(num.getText() + "8");
		} else if (e.getSource() == nueve) {
			if (num.getText() == "")
				num.setText("9");
			else
				num.setText(num.getText() + "9");
		} else if (e.getSource() == mas) {
			if (num.getText() == "")
				num.setText("+");
			else
				num.setText(num.getText() + "+");
		} else if (e.getSource() == menos) {
			if (num.getText() == "")
				num.setText("-");
			else
				num.setText(num.getText() + "-");
		} else if (e.getSource() == mult) {
			if (num.getText() == "")
				num.setText("*");
			else
				num.setText(num.getText() + "*");
		} else if (e.getSource() == div) {
			if (num.getText() == "")
				num.setText("/");
			else
				num.setText(num.getText() + "/");
		} else if (e.getSource() == borrar) {
			num.setText("");
			res.setText("");
		} else if (e.getSource() == cero) {
			if (num.getText() == "")
				num.setText("0");
			else
				num.setText(num.getText() + "0");
		} else if (e.getSource() == punto) {
			if (num.getText() == "")
				num.setText(".");
			else
				num.setText(num.getText() + ".");
		} else if (e.getSource() == not) {
			JOptionPane.showMessageDialog(null,
					"La calculadora solo funciona con un operador(por el momento), sino se rompe");
		}

		else if (e.getSource() == igual) {		//OPERADORES

			String cadena = num.getText();

			for (int i = 0; i < cadena.length(); i++) {
				char carac = cadena.charAt(i);
				if (carac == '+') {
					String primer = cadena.substring(0, i);
					String segun = cadena.substring(i + 1, cadena.length());

					double res = Double.parseDouble(primer) + Double.parseDouble(segun);
					this.res.setText(Double.toString(res));
				} else if (carac == '-') {
					String primer = cadena.substring(0, i);
					String segun = cadena.substring(i + 1, cadena.length());

					double res = Double.parseDouble(primer) - Double.parseDouble(segun);
					this.res.setText(Double.toString(res));
				} else if (carac == '*') {
					String primer = cadena.substring(0, i);
					String segun = cadena.substring(i + 1, cadena.length());

					double res = Double.parseDouble(primer) * Double.parseDouble(segun);
					this.res.setText(Double.toString(res));
				} else if (carac == '/') {
					String primer = cadena.substring(0, i);
					String segun = cadena.substring(i + 1, cadena.length());

					double cero = Double.parseDouble(segun);
					if (cero == 0) {
						num.setText("");
						JOptionPane.showMessageDialog(null, "No se puede dividir entre cero");
						
					} else {
						double res = Double.parseDouble(primer) / Double.parseDouble(segun);

						this.res.setText(Double.toString(res));
					}
				}

			}

		}

	}

}
