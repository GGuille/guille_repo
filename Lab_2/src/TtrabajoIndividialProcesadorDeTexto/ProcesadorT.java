package TtrabajoIndividialProcesadorDeTexto;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.WindowConstants;
import javax.swing.text.StyledEditorKit;

class ProcesadorT extends JFrame {

	public ProcesadorT() {
		setBounds(300, 100, 500, 650);
		Panel menu = new Panel();
		add(menu);
		setVisible(true);
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE); // Esto va para que el WindwowClosing no cierre
																		// todo cuando se presione cancelar
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				menu.Salir();
			}
		});

	}

}

class Panel extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JPanel barra = new JPanel();
	JMenuBar lmenu = new JMenuBar();

	JTextPane ptext = new JTextPane();
	JScrollPane stext = new JScrollPane(ptext);

	JMenu archivo, fuente, estilo, tamanio;
	JMenuItem nuevo, abrir, guardar, guardar_como, salir;

	public Panel() {

		setLayout(new BorderLayout());
		archivo = new JMenu("Archivo");
		fuente = new JMenu("Fuente");
		estilo = new JMenu("Estilo");
		tamanio = new JMenu("Tama�o");

		// --------------------------------------MENU ARCHIVO

		nuevo = new JMenuItem("Nuevo");
		archivo.add(nuevo);
		nuevo.addActionListener(this);

		abrir = new JMenuItem("Abrir");
		archivo.add(abrir);
		abrir.addActionListener(this);

		guardar = new JMenuItem("Guardar");
		archivo.add(guardar);
		guardar.addActionListener(this);

		guardar_como = new JMenuItem("Guardar como..");
		archivo.add(guardar_como);
		guardar_como.addActionListener(this);

		salir = new JMenuItem("Salir");
		archivo.add(salir);
		salir.addActionListener(this);

		// ---------------------------------------MENU FUENTE

		menuControl("Arial", "fuente", "Arial", 0, 0);
		menuControl("Courier", "fuente", "Courier", 0, 0);
		menuControl("Verdana", "fuente", "Verdana", 0, 0);

		// ------------------------------------------MENU ESTILO

		menuControl("Negrita", "estilo", "", Font.BOLD, 0);
		menuControl("Cursiva", "estilo", "", Font.ITALIC, 0);

		// ------------------------------------------MENU TAMA�O

		menuControl("12", "tamanio", "", 0, 12);
		menuControl("16", "tamanio", "", 0, 16);
		menuControl("20", "tamanio", "", 0, 20);
		menuControl("24", "tamanio", "", 0, 24);
		menuControl("28", "tamanio", "", 0, 28);

		// ---------------------------------------AGREGACION DE MENUES
		lmenu.add(archivo);
		lmenu.add(fuente);
		lmenu.add(estilo);
		lmenu.add(tamanio);

		barra.add(lmenu);
		add(barra, BorderLayout.NORTH);
		add(stext, BorderLayout.CENTER);
	}

	public void menuControl(String text, String menu, String letra, int estilos, int tama�o) {

		JMenuItem elemento = new JMenuItem(text);

		if (menu.equals("fuente")) {
			fuente.add(elemento);
			elemento.addActionListener(new StyledEditorKit.FontFamilyAction("cambioLetra", letra));
		} else if (menu.equals("estilo")) {
			estilo.add(elemento);

			if (estilos == Font.BOLD) {
				elemento.addActionListener(new StyledEditorKit.BoldAction());
				elemento.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));
			} else if (estilos == Font.ITALIC) {
				elemento.addActionListener(new StyledEditorKit.ItalicAction());
				elemento.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K, InputEvent.CTRL_DOWN_MASK));
			}

		} else if (menu.equals("tamanio")) {
			tamanio.add(elemento);
			elemento.addActionListener(new StyledEditorKit.FontSizeAction("cambioTama�o", tama�o));
		}

	}

	public void actionPerformed(ActionEvent e) { // ---------------------------------------------EVENTOS
		if (e.getSource() == nuevo) {
			Nuevo();
		} else if (e.getSource() == abrir) {
			Abrir();
		} else if (e.getSource() == guardar) {
			Guardar();
		} else if (e.getSource() == guardar_como) {
			Guardar_como();
		} else if (e.getSource() == salir) {
			Salir();
		}
	}

	// -------------------------------------------------METODOS DE EVENTOS

	
	File url = null;

	public void Nuevo() { // --------------------NUEVO ARCHIVO
		String doc = ptext.getText();

		if (!doc.equals("")) {
			int seleccion = JOptionPane.showOptionDialog(null, "Quiere guardar los cambios?", "Guardar",
					JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null,
					new Object[] { "Guardar", "No guardar", "Cancelar" }, "opcion 1");
			if (seleccion != -1) {
				if (seleccion == 0) {
					Guardar();
				} else if (seleccion == 1) {
					ptext.setText("");
				}
			}

		} else {

		}

	}

	// -----------METODOS DEL STREAM DE DATOS
	public void Abrir() { // ------ABRIR/LEER ARCHIVO
		String doc = "";

		JFileChooser fc = new JFileChooser();

		int seleccion = fc.showOpenDialog(this);
		if (seleccion == JFileChooser.APPROVE_OPTION) {

			url = fc.getSelectedFile();

			try {
				FileInputStream entrada = new FileInputStream(url);

				int caracter;

				while ((caracter = entrada.read()) != -1) {
					char letra = (char) caracter;
					doc += letra;
				}
				entrada.close();

			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, "Ha ocurrido un Error");
				System.out.println(e);
			}
		}
		ptext.setText(doc);

	}
	
	String gar;
	public void Guardar() { // -----------GUARDAR/ESCRIBIR ARCHIVO
		String doc = ptext.getText();

		if (url == null) { // SI EL ARCHIVO SE QUIERE GUARDAR(PERO NO ESTA CREADO U ABIERTO CON
							// ANTERIORIDAD)
			JFileChooser fc = new JFileChooser(); // NOS PREGUNTA LA RUTA DONDE QUEREMOS GUARDAR "ESTE NUEVO ARCHIVO"

			int seleccion = fc.showSaveDialog(this);
			if (seleccion == JFileChooser.APPROVE_OPTION) {

				url = fc.getSelectedFile();
				try {
					FileOutputStream salida = new FileOutputStream(url);

					for (int i = 0; i < doc.length(); i++) {
						salida.write(doc.charAt(i));
					}
					salida.close();

				} catch (IOException e) {
					JOptionPane.showMessageDialog(null, "Ha ocurrido un Error");
					System.out.println(e);
				}
			}
		} else { // SI EL ARCHIVO YA ESTA CREADO U ABIERTO CON ANTERIORIDAD
					// SE GUARDA EN ESA MISMA DIRECCION
			try {
				FileOutputStream salida = new FileOutputStream(url);

				for (int i = 0; i < doc.length(); i++) {
					salida.write(doc.charAt(i));
				}
				salida.close();

			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, "Ha ocurrido un Error");
				System.out.println(e);
			}
		}
		gar = doc;

	}

	public void Guardar_como() { // GUARDAR COMO/ESCRIBIR ARCHIVO
		String doc = ptext.getText();

		JFileChooser fc = new JFileChooser();

		int seleccion = fc.showSaveDialog(this);
		if (seleccion == JFileChooser.APPROVE_OPTION) {

			url = fc.getSelectedFile();

			try {
				FileOutputStream salida = new FileOutputStream(url);

				for (int i = 0; i < doc.length(); i++) {
					salida.write(doc.charAt(i));
				}

				salida.close();

			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, "Ha ocurrido un Error");
				System.out.println(e);
			}
		}
		gar = doc;
	}

	public void Salir() { // METODO PARA SALIR DEL PROGRAMA
		String doc = ptext.getText();
		
		
		if (doc.equals("") || doc.equals(gar)) {
			System.exit(0);
		}		else {
		
			int seleccion = JOptionPane.showOptionDialog(null, "Esta seguro de salir sin guardar?", "Salir",
					JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null,
					new Object[] { "Guardar", "No guardar", "Cancelar" }, "opcion 1");

			if (seleccion != -1) {
				if (seleccion == 1) {
					System.exit(0);
				} else if (seleccion == 0) {
					Guardar();
					System.exit(0);
				}
			}
		}
	}

}
