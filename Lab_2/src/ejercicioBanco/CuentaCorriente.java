package ejercicioBanco;

class CuentaCorriente {
	private String nombreTitular;
	private double saldo;
	private int nroCuenta;
	
	public CuentaCorriente(String nombreTitular, double saldo, int nroCuenta) {
		this.nombreTitular = nombreTitular;
		this.saldo = saldo;
		this.nroCuenta = nroCuenta;
	}

	public CuentaCorriente() {
		super();
	}

	public String getNombreTitular() {
		return nombreTitular;
	}

	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}

	public double getSaldo() {
		return saldo;
	}

	public void ingresarDinero(double dinero) {
		this.saldo = saldo+dinero;
	}
	
	public void sacarDinero(double dinero) {
		this.saldo = saldo-dinero;
	}

	public int getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(int nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	@Override
	public String toString() {
		return "CuentaCorriente [nombreTitular=" + nombreTitular + ", saldo=" + saldo + ", nroCuenta=" + nroCuenta
				+ "]";
	}
	
	
}
