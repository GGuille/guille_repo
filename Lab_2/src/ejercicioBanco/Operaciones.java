package ejercicioBanco;

class Operaciones {
	
	public static void Transferir(CuentaCorriente cuentaIngreso, CuentaCorriente cuentaEgreso, double monto){
        cuentaEgreso.sacarDinero(monto);
        cuentaIngreso.ingresarDinero(monto);
    }

}
