package interfazGrafica;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class interfaz {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		frame ventana = new frame();
		ventana.setVisible(true);

	}

}

class frame extends JFrame {

	private JTextField field1 = new JTextField();
	private JLabel tex1 = new JLabel("Campo 1: ");
	
	private JTextField field2 = new JTextField();
	private JLabel tex2= new JLabel("Campo 2: ");

	public frame() {
		setTitle("Eventos de Foco");
		setBounds(30, 30, 600, 500);
		setLayout(null);
		
		tex1.setBounds(30, 30, 70, 25);
		add(tex1);
		field1.setBounds(95, 30, 140, 28);
		add(field1);
		
		tex2.setBounds(30, 60, 70, 25);
		add(tex2);
		field2.setBounds(95, 60, 140, 28);
		add(field2);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Foco evento = new Foco();

		field1.addFocusListener(evento);
		field2.addFocusListener(evento);
	}

	class Foco implements FocusListener {

		@Override
		public void focusGained(FocusEvent e) {
			// TODO Auto-generated method stub
			if (e.getSource() == field1) {
				System.out.println("El campo 1 esta focuseado");
			} else {
				System.out.println("El campo 2 esta focuseado");
			}
		}

		@Override
		public void focusLost(FocusEvent e) {
			// TODO Auto-generated method stub
			System.out.println("Cambio de focus");
		}

	}

}